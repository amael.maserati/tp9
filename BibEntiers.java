import java.util.*;

public class BibEntiers {
    public List<Integer> occurences(List<Integer> liste) {
        List<Integer> occurences = new ArrayList<>();
        Set<Integer> nombresUniques = new HashSet<>(liste);
        for (Integer elem : nombresUniques) {
            int cpt = Collections.frequency(liste, elem);
            occurences.add(cpt);
        }
        return occurences;
    }
    public boolean memeElements(List<Integer> liste1, List<Integer> liste2) {
        if (liste1.size() != liste2.size()) { return false; }
        Collections.sort(liste1);
        Collections.sort(liste2);
        return liste1.equals(liste2);
    }
    public static List<Integer> fusionTriee(List<Integer> liste1, List<Integer> liste2) {
        List<Integer> listeTrieeFusionnee = new ArrayList<>();
        int i = 0, j = 0;
        while (i < liste1.size() && j < liste2.size()) {
            if (liste1.get(i) < liste2.get(j)) {
                listeTrieeFusionnee.add(liste1.get(i));
                i++;
            } else {
                listeTrieeFusionnee.add(liste2.get(j));
                j++;
            }
        }
        while (i < liste1.size()) {
            listeTrieeFusionnee.add(liste1.get(i));
            i++;
        }
        while (j < liste2.size()) {
            listeTrieeFusionnee.add(liste2.get(j));
            j++;
        }
        return listeTrieeFusionnee;
    }
    public boolean existeSommeNulle(List<Integer> liste) {
        for (int i = 0; i < liste.size(); ++i ) {
            for (int j = 1; j < liste.size(); ++j) {
                if (liste.get(i) + liste.get(j) == 0) {
                    return true;
                }
            }
        }
        return false;
    }
    public int plusPetiteDifference(List<Integer> liste) {
        Collections.sort(liste);
        int plusPetiteDifference = liste.get(1) - liste.get(0);
        for (int i = 2; i < liste.size(); ++i) {
            if (liste.get(i) - liste.get(i-1) < plusPetiteDifference) {
                plusPetiteDifference = liste.get(i) - liste.get(i-1);
            }
        }
        return plusPetiteDifference;
    }
}
