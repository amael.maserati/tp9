import java.util.*;

public class ComparateurComplexesIntervalle implements Comparator<Complexe> {
    public static boolean allInBounds(List<Complexe> liste, Comparator<Complexe> comparateur, Complexe c1, Complexe c2) {
        for (Complexe c : liste) {
            if (c2.getPremier() < c.getPremier() || c.getPremier < c1.getPremier() 
            || c2.getSecond < c.getSecond() || c.getSecond() < c1.getSecond()) {
                return false;
            }
        }
        return true;
    }
}